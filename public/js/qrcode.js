const button = document.getElementById("sendButton");
const qrcodeField = document.getElementById("qrcode");

button.addEventListener('click', (e) => {
  e.preventDefault();
  const discount = document.getElementById("discountLabel").value;
  var myHeaders = new Headers();
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  let send = "";
  if (discount !== "") {
    send = "?discountCode=" + discount;
  }
  fetch("http://localhost:3000/utils/qrcode" + send, requestOptions)
    .then(response => response.text())
    .then(result => {
      qrcodeField.setAttribute('src', result);
    })
    .catch(error => console.log('error', error));
})
