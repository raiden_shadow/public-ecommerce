const SignUpButton = document.getElementById("submit-signup")
const response = document.getElementById("result")

SignUpButton.addEventListener("click", (e) => {
  e.preventDefault()
  const username = document.getElementById("username-signup").value
  const nickname = document.getElementById("nickname-signup").value
  const password = document.getElementById("password-signup").value
  const email = document.getElementById("email-signup").value
  //Starting the request
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "username":username,
    "nickname":nickname,
    "password":password,
    "email":email
  });
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("http://localhost:3000/user/register", requestOptions)
  .then(response => response.text())
  .then(result => {
    result = JSON.parse(result)
    if (result.status) {
      window.location.href = "http://localhost:3000/user/register/verify"
    }
  })
  .catch(error => {
    console.log(error)
  });
})
