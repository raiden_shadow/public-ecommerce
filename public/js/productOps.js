const button = document.getElementById("submit")

button.addEventListener('click', (e) => {
  e.preventDefault()
  const name = document.getElementById("name").value
  const price = document.getElementById("price").value
  const amount = document.getElementById("amount").value
  const category = document.getElementById("category").value
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify({"name":name,"price":price,"amount":amount, "category": category});
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("http://localhost:3000/product/new", requestOptions)
    .then(response => response.text())
    .then(result => {})
    .catch(error => console.log('error', error));

})
