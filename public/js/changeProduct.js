const selectionArray = document.querySelector("#products")
const button = document.getElementById("submit")
const name = document.getElementById("Name")
const price = document.getElementById("Price")
const category = document.getElementById("Category")
const amount = document.getElementById("Amount")

var requestOptions = {
  method: 'GET',
  redirect: 'follow'
};

fetch("http://localhost:3000/products", requestOptions)
  .then(response => response.text())
  .then(result => {
    test = result = JSON.parse(result)
    for (let i = 0; i < result.length; i++) {
      const option = document.createElement("option")
      option.textContent = result[i].name
      option.setAttribute("value", result[i]._id)
      option.setAttribute("name", result[i].name)
      option.setAttribute("price", result[i].price)
      option.setAttribute("category", result[i].category)
      option.setAttribute("amount", result[i].amount)
      selectionArray.appendChild(option)
    }
    name.value = selectionArray[0].attributes.name.nodeValue
    price.value = selectionArray[0].attributes.price.nodeValue
    category.value = selectionArray[0].attributes.category.nodeValue
    amount.value = selectionArray[0].attributes.amount.nodeValue
  })
  .catch(error => console.log('error', error));

  $("#products").change(function() {
    if ($("#products option").is(":selected")) {
      name.value = $("#products option:selected")[0].attributes.name.nodeValue
      price.value = $("#products option:selected")[0].attributes.price.nodeValue
      category.value = $("#products option:selected")[0].attributes.category.nodeValue
      amount.value = $("#products option:selected")[0].attributes.amount.nodeValue
    } else {

    }
  });
button.addEventListener("click", (e) => {
  e.preventDefault()
  const fetching = document.getElementById("result")
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "name": name.value,
    "price": price.value,
    "category": category.value,
    "amount": amount.value
  });

  var requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  fetching.textContent = "Loading..."
  let id = $("#products option:selected")[0].attributes.value.nodeValue
  fetch("http://localhost:3000/product/change?id=" + id, requestOptions)
    .then(response => response.text())
    .then(result => {
      // result = JSON.parse(result)
      fetching.textContent = "Result!"
      // console.log(result)
      location.reload();
    })
    .catch(error => console.log('error', error));
})
