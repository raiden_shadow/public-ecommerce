const button = document.getElementById("send");

button.addEventListener('click', (e) => {
  e.preventDefault();
  const inputValue = document.getElementById("codeInput").value;
  let myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify({
    "code":inputValue
  });
  let requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow"
  }
  fetch("http://localhost:3000/user/register/verify", requestOptions)
  .then(response => response.text())
  .then(result => {
    result = JSON.parse(result);
    console.log(result);
  })
  .catch(e => {
    console.log(e);
  });
});
