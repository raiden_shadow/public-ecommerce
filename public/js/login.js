var myHeaders = new Headers();
var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("http://localhost:3000/user/isLoggedIn", requestOptions)
.then(response => response.text())
.then(result => {
  result = JSON.parse(result)
  if (result.status) {
    window.location.href = "http://localhost:3000/user/panel"
  } else {

  }
})
.catch(error => console.log('error', error));

const LoginButton = document.getElementById("submit-login");

LoginButton.addEventListener('click', (e) => {
  e.preventDefault()
  const userName = document.getElementById("username-login").value
  const password = document.getElementById("password-login").value
  const avatar = document.getElementById("avatar")
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify({"username": userName,"password": password});
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("http://localhost:3000/user/login", requestOptions)
    .then(response => response.text())
    .then(result => {
      result = JSON.parse(result)
      window.location.href = "http://localhost:3000/user/panel"
    }).catch(error => console.log('error', error));
})



const LacksAccount = document.getElementById('lacks-account');
const HasAccount = document.getElementById('has-account');
const SignUpDiv = document.getElementById('signup-box-div');
const LoginDiv = document.getElementById("login-box-div");
LacksAccount.addEventListener('click', (e) => {
  LoginDiv.style.display = "none";
  SignUpDiv.style.display = "block";
})
HasAccount.addEventListener('click', (e) => {
  LoginDiv.style.display = "block";
  SignUpDiv.style.display = "none";
})
