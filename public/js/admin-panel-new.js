const DataCollection = document.getElementsByClassName('data');
const okbtn = document.getElementById('ok');

const Overview = {
    btn: document.getElementById('overview-option'),
    data: document.getElementById('overview-data'),
    dataShow () {
        for(var i = 0 ; i<DataCollection.length; i++){
            DataCollection[i].classList.add('hide-data');
        }
        this.data.classList.remove('hide-data');
    },
    btnOnclick: 
    document.getElementById('overview-option').addEventListener('click', () => {
        Overview.dataShow();
        console.log('12345');
    }),
}
const Products = {
    btn: document.getElementById('products-option'),
    data: document.getElementById('products-data'),
    dataShow () {
            for(var i = 0 ; i<DataCollection.length; i++){
                DataCollection[i].classList.add('hide-data');
            }
            this.data.classList.remove('hide-data');
            //Query selection for all tiles in the page//
            const productTiles = document.querySelectorAll(".product-tile")
            var myHeaders = new Headers();
            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            fetch("http://localhost:3000/products", requestOptions)
                .then(response => response.text())
                .then(result => {
                            result = JSON.parse(result)
                            for (let i = 0; i < result.length; i++) {
                                productTiles[i].childNodes[3].childNodes[1].childNodes[0].nodeValue = result[i].name
                            }
                    })
                            .catch(error => console.log('error', error));},
    btnOnclick: document.getElementById('products-option').addEventListener('click', () => {
        Products.dataShow();
        //event testing 
        console.log('clicked on Products');
    }),
}
const Customers = {
    btn: document.getElementById('customers-option'),
    data: document.getElementById('customers-data'),
    dataShow () {
        for(var i = 0 ; i<DataCollection.length; i++){
            DataCollection[i].classList.add('hide-data');
        }
        this.data.classList.remove('hide-data');
    },
    btnOnclick: document.getElementById('customers-option').addEventListener('click', () => {
        Overview.dataShow();
        //event testing 
        console.log('clicked on Customers');
    }),
}
const Summary = {
    btn: document.getElementById('summary-option'),
    data: document.getElementById('summary-data'),
    dataShow () {
        for(var i = 0 ; i<DataCollection.length; i++){
            DataCollection[i].classList.add('hide-data');
        }
        this.data.classList.remove('hide-data');
    },
    btnOnclick: document.getElementById('summary-option').addEventListener('click', () => {
        Overview.dataShow();
        //event testing 
        console.log('clicked on summary option');
    }),
}
const Settings = {
    btn: document.getElementById('settings-option'),
    data: document.getElementById('settings-data'),
    dataShow () {
        for(var i = 0 ; i<DataCollection.length; i++){
            DataCollection[i].classList.add('hide-data');
        }
        this.data.classList.remove('hide-data');
    },
    btnOnclick: document.getElementById('settings-option').addEventListener('click', () => {
        Overview.dataShow();
        //event testing 
        console.log('clicked on settings option');
    }),
}

const Logout = {
    btn: document.getElementById('logout-option'),
    btnOnclick: document.getElementById('logout-option').addEventListener('click', (e) => {
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = "";
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        fetch("http://localhost:3000/user/logout", requestOptions)
            .then(response => response.text())
            .then(result => {
                result = JSON.parse(result)
                if (result.status) {
                window.location.href = "http://localhost:3000/user"
                }
            })
          .catch(error => console.log('error', error));
        
          //event testing 
        console.log('clicked on Logout option');
        
    }),
}
// fetcing avatar and user information for panel
const nickname = document.getElementById("nickname")
const avatar = document.getElementById("avatar")
var requestOptions = {
    method: 'GET',
    redirect: 'follow'
};
fetch("http://localhost:3000/user/me", requestOptions)
    .then(response => response.text())
    .then(result => {
        result = JSON.parse(result)
        nickname.textContent = result.nickname
        avatar.setAttribute("src", result.thumbnail)
    })
    .catch(error => console.log('error', error));


// modal managament
const ModalErrorWindow = document.getElementById('modal-error-404');
okbtn.addEventListener('click', () => {
    ModalErrorWindow.classList.add('hide-data');
});
