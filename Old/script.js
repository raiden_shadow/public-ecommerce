var isToggled = false;
$('.uiButton').click(function() {
    if(isToggled==false){
        $('.uiButton').removeClass("btn-secondary");
        $(this).addClass("btn-primary");
        isToggled = true;
    } else {
        $('.uiButton').removeClass("btn-primary");
        $(this).addClass("btn-secondary");
        isToggled = false;
    }
  });