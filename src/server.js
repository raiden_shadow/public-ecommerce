//Importing necessaries
  const express = require("express")
  // const https = require("https") For later
  const path = require("path")
  const hbs = require("hbs")
  const productRouter = require("./routers/product")
  const userRouter = require("./routers/user")
  const utilRouter = require("./routers/utils")
  const cookieParser = require("cookie-parser")
  const cors = require("cors")
//Paths
  const publicPath = path.join(__dirname, '../public')
  const viewPath = path.join(__dirname, '../templates')
//Getting started
  const app = express()
  const port = process.env.PORT
  app.use(cors({
    origin: /localhost\:1337$/,
    credentials: true
  }))
  app.use(cookieParser())
  app.use(express.json())
  app.use(productRouter)
  app.use(userRouter)
  app.use(utilRouter)
  app.set('view engine', 'hbs')
  app.set('views', viewPath)
  app.use(express.static(publicPath))
//Homepage
  app.get('/', (req, res) => {
    res.render("index", {})
  })
//Starting the server on the port 3000
  app.listen(port, () => {
    console.log("Server is running on port " + port)
  })
