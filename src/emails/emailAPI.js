const sgMail = require("@sendgrid/mail")

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: "alirezakiaei@gmail.com",
    subject: "Welcome!",
    text: `Hello, ${name}`
  })
}

const sendVerificationEmail = (email, name, code) => {
  sgMail.send({
    to: email,
    from: "alirezakiaei@gmail.com",
    subject: "Verification",
    text:
    `Hello, Dear ${name}.
      Here is your verification code: ${code}
    `
  })
}


module.exports = {
  sendEmail,
  sendVerificationEmail
}
