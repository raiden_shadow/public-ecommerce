const { clearCache } = require("../services/cache");

const applyToCache = async (req, res, next) => {
  await next();

  await clearCache(req.user._id);
};


module.exports = {applyToCache};
