//Setups
const express = require("express");
require("../db/database");
const Product = require("../models/product");
const router = new express.Router();
const auth = require("../middleware/auth");
const authCritical = require("../middleware/authCritical");
const { applyToCache } = require("../middleware/cacheWare");
//APIs
//Search for all products
router.get("/products", auth, async (req, res) => {
  try {
    const find = await Product.find({}).cache({ key: req.user._id });
    res.status(200).send(find);
  } catch (e) {
    res.status(500).send(e);
  }
});
//Search for specified product
router.get("/product", auth, async (req, res) => {
  try {
    const queries = Object.keys(req.query);
    const queryValues = Object.values(req.query);
    if (queries.length == 0) {
      try {
        const products = await Product.find({});
        res.status(202).send(products);
      } catch (e) {
        res.status(500).send(e);
      }
    } else {
      const allowedOnes = ["name", "price", "category", "barcode"];
      const isValid = queries.every((each) => {
        return allowedOnes.includes(each);
      })
      if (isValid) {
        const search = queries.reduce((a, key) => Object.assign(a, { [key]: null }), {});
        for (let i = 0; i < queries.length; i++) {
          if (queries[i] != "price") {
            search[queries[i]] = new RegExp(`${queryValues[i]}`, 'i');
            continue;
          }
          search[queries[i]] = queryValues[i];
        }
        const found = await Product.find(search);
        if (found.length == 0) {
          res.status(404).send({ error: true });
        }
        if (!found) {
          res.status(404).send({ error: true });
        }
        res.status(202).send(found);
      }
    }
    res.status(400).send({ error: "Didn't request correctly!" });
  } catch (e) {
    res.status(500).send(e);
  }
});
//Sort by
router.get("/products/sort", async (req, res) => {
  try {
    const queries = Object.keys(req.query);
    const queryValues = Object.values(req.query);
    if (queries.length > 1 && queries[0] == "by") {
      const whatObj = {
        [queryValues[0]]: req.query.type
      }
      const products = await Product.find({}).sort(whatObj).skip(2);
      res.status(200).send(products);
    } else {
      res.status(400).send();
    }
    res.status(200).send();
  } catch (e) {

  }
});
//Product page
router.get("/product/new", authCritical, async (req, res) => {
  try {
    res.render('addProduct', {});
  } catch (e) {
    res.status(500).send(e);
  }
});
//Insert a product
router.post("/product/new", authCritical, applyToCache, async (req, res) => {
  try {
    const creating = new Product(req.body);
    await creating.save();
    res.status(201).send({ result: "Product created successfully!"});
  } catch (e) {
    res.status(400).send(e);
  }
});
//Change page
router.get("/product/change", authCritical, async (req, res) => {
  try {
    res.render('changeProduct', {});
  } catch (e) {
    res.status(500).send(e);
  }
})
//Update
router.patch("/product/change", authCritical, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "price", "amount", "imageURL", "barcode", "category"];
  const isValid = updates.every((update) => {
    return allowedUpdates.includes(update);
  })
  if (!isValid) {
    return res.status(400).send({ error: "Invalid entries!"});
  }
  try {
    const specifiedProduct = await Product.findByIdAndUpdate(req.query.id, req.body, { new: true, runValidators: true });
    if (!specifiedProduct) {
      return res.status(404).send("Not found!");
    }
    res.status(202).send(specifiedProduct);
  } catch (e) {
    res.status(500).send({ error: "error" });
  }
});
//Delete a document
router.delete("/product/remove", authCritical, async (req, res) => {
  try {
    const deleteADocument = await Product.findByIdAndDelete(req.query.id);
    if (!deleteADocument) {
      return res.status(404).send("Not Found!");
    }
    res.status(202).send("Task completed successfully!");
  } catch (e) {
    res.status(500).send(e);
  }
});
//Updating price history all products every hour (Automatically)
const updateDB = async () => {
  setTimeout(async () => {
    const findAnProdcut = await Product.find({});
    for (let i = 0; i < findAnProdcut.length; i++) {
      const currentOne = findAnProdcut[i];
      if (findAnProdcut[i].pricehistory.length >= 3) {
        const priceRemover = await currentOne.priceRemover();
        continue;
      }
      const priceSaver = await currentOne.priceSaver();
    }
    updateDB();
  }, 3600000);
};

updateDB();

//Export the APIs
module.exports = router;
