//Setups
const express = require("express");
require("../db/database");
const router = new express.Router();
const QRCode = require("qrcode");
const cryptoRandomString = require('crypto-random-string');
//APIs
//Render utilities page
router.get('/utils', (req, res) => {
  res.render("test", {});
});
//Get qrcode API
router.get('/utils/qrcode', async (req, res) => {
  let discount = cryptoRandomString({ length: 5, characters: 'distinguishable'})
  const queries = Object.keys(req.query);
  if (queries.length !== 0) {
    if (queries.includes("discountCode")) {
      discount = req.query.discountCode
    }
  }
  const myQrCode = await QRCode.toDataURL(discount);
  res.status(200).send(myQrCode);
});


module.exports = router;
