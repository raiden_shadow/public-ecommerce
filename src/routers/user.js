//Setups
const express = require("express");
const path = require("path");
const validator = require("validator");
require("../db/database");
const User = require("../models/accounts");
const router = new express.Router();
const auth = require("../middleware/auth");
const authCritical = require("../middleware/authCritical");
const multer = require("multer");
const fs = require("fs");
const directory = "/img/";
const util = require("util");
const { sendVerificationEmail, sendEmail } = require("../emails/emailAPI");
const cache = require("../services/cache");
//Settings for upload route
const upload = multer({
  dest: 'public/img',
  limits: {
    fileSize: 500000,
  },
  fileFilter(req, file, cb){
    if (!file.originalname.match(/\.(jpg|png|jpeg)$/)) {
      return cb(new Error("Please upload an image with jpg, png or jpeg format."));
    }
    cb(undefined, true);
  }
});
//APIs
//Panel
router.get("/user/panel", auth, async (req, res) => {
  try {
    if (req.user.isAdmin === false) {
      res.render("userPanel", {});
    } else {
      res.render("adminPanel", {});
    }
  } catch (e) {
    res.status(500).send();
  }
});
//Login page
router.get("/user", async (req, res) => {
  try {
    res.render("userAuth", {});
  } catch (e) {
    res.status(500).send("An error occured!");
  }
});
//Check if logged-in
router.post('/user/isLoggedIn', auth, async (req, res) => {
  try {
    res.status(200).send({ status: "true" });
  } catch (e) {
    res.status(500).send({ status: "false" });
  }
});
//Profile info
router.get('/user/me', auth, async (req, res) => {
  try {
    res.status(200).send(req.user);
  } catch (e) {
    res.status(500).send();
  }
});
//Sign-up - 2 Step
//Step 1
router.post("/user/register", async (req, res) => {
  try {
    if (validator.isEmail(req.body.email)) {
      const theCode = await cache.verificationCode(req.body.email, "signUp");
      await cache.cacheIt(req.body, true, null);
      sendVerificationEmail(req.body.email, req.body.nickname, theCode);
      res.status(200).send({ status: true });
    } else {
      throw new Error({ error: "Enter a valid email!" });
    }
  } catch (e) {
    res.status(400).send(e);
  }
});
// Step 2
router.get("/user/register/verify", async (req, res) => {
  res.render("verify", {});
})
router.post("/user/register/verify", async (req, res) => {
  const comingCode = req.body.code;
  req.body = await cache.cacheIt(null, false);
  try {
    const state = await cache.checkForValidation(req.body.email, "signUp", comingCode);
    if (state) {
      const creating = new User(req.body);
      await creating.save();
      const token = await creating.generateAuthToken();
      res.cookie('Authorization', 'Bearer ' + token, {
        expires: new Date(Date.now() + 24 * 3600000) // cookie will be removed after 24 hours
      });
      res.status(201).send({ creating, token });
      await clearCache(req.body.email);
    } else {
      res.status(406).send({ status: false });
    }
  } catch (e) {
    res.status(400).send({ status: false, e });
  }
});
//Sign-in
router.post("/user/login", async (req, res) => {
  try {
    const account = await User.findByCredentials(req.body.username, req.body.password);
    const token = await account.generateAuthToken();
    res.cookie('Authorization', 'Bearer ' + token, {
    expires: new Date(Date.now() + 24 * 3600000) // cookie will be removed after 24 hours
  }).status(202).send({ account, token });
  } catch (e) {
    res.status(400).send({ error: e });
  }
});
//Sign-out
router.post("/user/logout", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();
    res.clearCookie("Authorization");
    res.status(202).send({ status : true });
  } catch (e) {
    res.status(500).send();
  }
});
//Sign-out all other sessions
router.post("/user/logoutAll", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token === req.token;
    });
    await req.user.save();

    res.status(200).send("All other sessions have been removed successfully");
  } catch (e) {
    res.status(500).send();
  }
});
//Update self-account
router.patch("/user/me", auth, async (req, res) => {
  const updateEntries = Object.keys(req.body);
  const allowedUpdates = ["thumbnail", "email", "password", "username"];
  const isValid = updateEntries.every((entry) => {
    return allowedUpdates.includes(entry);
  })
  if (!isValid) {
    return res.status(400).send({ error: "Invalid entries!"});
  }
  try {
    const myAccount = await User.findByIdAndUpdate(req.user._id, req.body, { new: true, runValidators: true});
    res.status(202).send(myAccount);
  } catch (e) {
    res.status(500).send("Something went wrong!");
  }
});
//Delete self-account
router.delete("/user/me", auth, async (req, res) => {
  try {
    await User.findOneAndDelete({ _id: req.user._id });
    res.status(202).send("Account deleted successfully!");
  } catch (e) {
    res.status(400).send();
  }
});
//Update an account (Other accounts)
router.patch("/user/change", authCritical, async (req, res) => {
  //
  const updateEntries = Object.keys(req.body[1]);
  const allowedUpdates = ["isAdmin", "thumbnail", "email", "password", "username"];
  const isValid = updateEntries.every((entry) => {
    return allowedUpdates.includes(entry);
  })
  if (!isValid) {
    return res.status(400).send({ error: "Invalid entries!"});
  }
  try {
    const user = await User.findOne({ username: req.body[0].username });
    if (!user) {
      return res.status(404).send();
    }
    const specifiedAccount = await User.findByIdAndUpdate(user._id, req.body[1], { new: true, runValidators: true});
    res.status(202).send(specifiedAccount);
  } catch (e) {
    res.status(500).send("Something went wrong!");
  }
});
//Delete an account (Other accounts)
router.delete("/user/admin/delete", authCritical, async (req, res) => {
  try {
    const user = await User.findOneAndDelete({ username: req.body.username, email: req.body.email });
    if (!user) {
      return res.status(404).send();
    }
    res.status(202).send("Account deleted successfully!");
  } catch (e) {
    res.status(400).send();
  }
});
//Upload route
router.post("/user/me/avatar", auth, upload.single("avatar"), async (req, res) => {
  const filePath = path.join(__dirname, "../../public/img/");
  fs.rename(filePath + req.file.filename , filePath + req.file.originalname, function(err) {
    if ( err ) console.log('ERROR: ' + err);
  });
  req.user.thumbnail = directory + req.file.originalname;
  await req.user.save();
  console.log(filePath);
  res.status(200).send({ result: "File uploaded"});
}, (error, req, res, next) => {
  res.status(400).send({ error: error.message });
});
//Delete avatar
router.delete("/user/me/avatar", auth, async (req, res) => {
  try {
    fs.unlinkSync(req.user.thumbnail);
    req.user.thumbnail = "none";
    res.status(200).send("File deleted successfully!");
  } catch (e) {
    res.status(400).send("Didn't find any thumbnail for this user");
  }
});

//Export the APIs
module.exports = router;
