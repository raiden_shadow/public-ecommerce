const mongoose = require("mongoose")
const jwt = require("jsonwebtoken")

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  price: {
    type: Number,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  imageAddress:{
    type: String,
    required: false,
  },
  barcode: {
    type: Number,
    required: false,
    default: 0
  },
  category: {
    type: String,
    required: true,
    trim: true
  },
  pricehistory: [{
    price: {
      type: Number
    },
    date: {
      type: Date
    }
  }]
}, {
  timestamps: true
})

productSchema.methods.priceSaver = async function() {
  const product = this
  const price = product.price
  product.pricehistory = product.pricehistory.concat({ price, date: new Date() })
  await product.save()
  return product.pricehistory
}

productSchema.methods.priceRemover = async function() {
  const product = this
  product.pricehistory.shift()
  await product.save()
  return product.pricehistory
}

const Product = mongoose.model("Product", productSchema)

module.exports = Product
