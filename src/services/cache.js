//Imports
const mongoose = require("mongoose");
const redis = require("redis");
const util = require("util");
const crypt = require("crypto-random-string");
//Setup
const redisURL = "redis://127.0.0.1:6379";
const client = redis.createClient(redisURL);
client.hget = util.promisify(client.hget);
client.hgetall = util.promisify(client.hgetall);
client.get = util.promisify(client.get);
const exec = mongoose.Query.prototype.exec;


//For setup the cache method for query
mongoose.Query.prototype.cache = async function(options = {}){
  this.useCache = true;
  this.hashKey = JSON.stringify(options.key || "");

  return this;
}

//Override the exec query function
mongoose.Query.prototype.exec = async function () {
  if (!this.useCache) {
    return exec.apply(this, arguments);
  }
  const key = JSON.stringify(Object.assign({}, this.getOptions(), {
    collection: this.mongooseCollection.name
  }));

  const cachedNode = await client.hget(this.hashKey, key);

  if (cachedNode) {
    console.log("Getting from cache!");
    const forCheck = JSON.parse(cachedNode);
    return Array.isArray(forCheck) ? forCheck.map(go => new this.model(go)) : new this.model(forCheck) ;
  }

  const result = await exec.apply(this, arguments);
  console.log("Giving to the cache!");
  client.hset(this.hashKey, key, JSON.stringify(result));

  return result;
};

let cacheIt = async (input, toggle) => {
  //toggle = true -> set
  //toggle = false -> get
  if (toggle) {
    client.set("dummy", JSON.stringify(input));
  } else {
    const output = await client.get("dummy");
    return JSON.parse(output);
  }
}

let clearCache = (hashKey) => {
  client.del(JSON.stringify(hashKey));
  client.del("dummy");
}

let verificationCode = async (input, forWhat) => {
  const randomCode = crypt({length: 6, type: 'distinguishable'});
  await client.hset(JSON.stringify(input), forWhat, randomCode);
  return randomCode;
}

let checkForValidation = async (input, forWhat, code) => {
  const check = await client.hget(JSON.stringify(input), forWhat);
  if (check == code) {
    return true;
  }
  return false;
}

module.exports = {
  //Cache something
  cacheIt,
  //Function that clear cache for an specific hashkey
  clearCache,
  //Function for creating verification code
  verificationCode,
  //Checking the code
  checkForValidation
};
